import { showMessage } from 'test-app/helpers/show-message';
import { module, test } from 'qunit';

module('Unit | Helper | show message');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = showMessage([42]);
  assert.ok(result);
});
